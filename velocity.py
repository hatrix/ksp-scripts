from math import sqrt


def orbital_velocity(mass, radius_body, perigee, apogee):
    # Returns velocity at perigee, apogee
    G = 6.674e-11

    # km to m
    a = (apogee + radius_body) * 1000
    p = (perigee + radius_body) * 1000

    v_perigee = sqrt(G * mass * ((2 / p) - (1 / a)))
    v_apogee = sqrt(G * mass * ((2 / a) - (1 / a)))

    return v_perigee, v_apogee


if __name__ == "__main__":
    mass = float(eval(input("Mass: ")))
    body_radius = float(eval(input("Planet's radius in km: ")))
    perigee = float(eval(input("Perigee in km: ")))
    apogee = float(eval(input("Apogee in km: ")))

    v_perigee, v_apogee = orbital_velocity(mass, body_radius, perigee, apogee)
    print("\nDo not forget to substract the angular velocity if landing on the",
          " equator!")
    print("Orbital velocity at:")
    print(f"  Apogee : {v_apogee:.0f} m/s")
    print(f"  Perigee: {v_perigee:.0f} m/s")
