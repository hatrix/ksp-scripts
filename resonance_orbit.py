# Given a planet mass, an orbit altitude and a number of satellites, returns
# the apogee and perigee needed for insertions

from math import pi


def orbital_period(mass_planet, radius_planet, altitude, nb_satellites):
    '''Mass in kg
       Altitude in km
       radius in km
       Number int

       op in seconds'''

    a = (altitude + radius_planet)
    GM = 6.67430e-11 * mass_planet
    op = 2 * pi * ((a ** 3) / GM) ** (1/2)

    return op, op + op * (1 / nb_satellites)
 

def get_perigee_apogee(mass_planet, orbital_period, radius_planet, altitude):
    GM = 6.67430e-11 * mass_planet
    T = orbital_period

    semi_major_axis = ((GM * T ** 2) / (4 * pi ** 2)) ** (1/3)
    semi_major_axis = semi_major_axis

    perigee = altitude
    apogee = semi_major_axis + semi_major_axis - altitude - radius_planet * 2

    return round(perigee, 3), round(apogee, 3)


def format_orbital_period(op, type_orbit):
    hours = int(op // 3600)
    minutes = int((op - hours * 3600) // 60)
    seconds = round(op - hours * 3600 - minutes * 60, 2)
    return f'{type_orbit} period: {hours}h:{minutes}m:{seconds}s'


if __name__ == "__main__":
    mass = float(input("Mass of planet in kg: "))
    radius = float(input("Radius of planet in km: ")) * 1000
    altitude = float(input("Desired orbit in km: ")) * 1000
    nb_satellites = float(input("Number of satellites: "))

    op_sync, op_res = orbital_period(mass, radius, altitude, nb_satellites)
    perigee, apogee = get_perigee_apogee(mass, op_res, radius, altitude)

    print(format_orbital_period(op_res, 'Resonant orbit'), op_res)
    print(format_orbital_period(op_sync, 'Circular orbit'), op_sync, '\n')
    print(f'Perigee (Injection): {perigee} m')
    print(f'Apogee: {apogee} m')
