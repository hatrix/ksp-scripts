from math import log

def d_v(start_mass, end_mass, isp):
    return int(log(start_mass / end_mass) * isp * 9.81)

if __name__ == "__main__":
    start_mass = float(eval(input("Start mass: ")))
    end_mass = float(eval(input("End mass: ")))
    isp = float(eval(input("ISP: ")))

    print(d_v(start_mass, end_mass, isp))
