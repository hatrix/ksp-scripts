def to_sec(d, h, m, s):
    seconds = d * 6 * 60 * 60 + h * 60 * 60 + m * 60 + s
    return seconds

def to_days(s):
    days = int(s // (6 * 3600))
    hours = int((s - days * 6 * 3600) // 3600)
    minutes = int((s - days * 6 * 3600 - hours * 3600) // 60)
    seconds = round(s - days * 6 * 3600 - hours * 3600 - minutes * 60, 2)
    return f'Period: {days}d {hours}h {minutes}m {seconds}s'


if __name__ == "__main__":
    print(to_sec(2, 1, 8, 0))
    print(to_days(47390))


