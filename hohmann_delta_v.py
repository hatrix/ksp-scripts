# Returns the Δv required to go from one orbit to the other one
# From 2 to 3 here 
# https://upload.wikimedia.org/wikipedia/commons/8/8c/Hohmann_transfer_orbit2.svg

from math import sqrt

def hohmann_dv(mass_planet, radius_planet, apogee, perigee):
    GM = 6.67430e-11 * mass_planet
    r1 = perigee + radius_planet
    r2 = apogee + radius_planet

    dv = sqrt(GM/r2) * (1 - sqrt((2 * r1) / (r1 + r2)))

    return round(dv, 2)


if __name__ == "__main__":
    # Kerbin example

    mass = float(input("Planet mass in kg: "))
    radius = float(input("Planet radius in km: "))
    perigee = float(input("Perigee in km: "))
    apogee = float(input("Apogee in km: "))

    perigee = perigee * 1000
    apogee = apogee * 1000
    radius = 600 * 1000

    dv = hohmann_dv(mass, radius, apogee, perigee)
    print(f'{dv} m/s')
