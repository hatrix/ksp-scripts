# Relay network around Rhode

## Geostationnary orbit

With:

  * $T$: sidereal period of the planet in seconds
  * $M$: mass of the planet in kg
  * $r$: radius of the planet in meters
  * $G$: gravitational constant, $6.67430 \times 10^{-11}$
  * $altitude$: in meters

$$ altitude = \sqrt[3]{\frac{G M T^2}{4 \pi^2}} - r$$

### Numerical application

The days in KSP are 6 hours long. To convert from days/minutes to seconds we
should not forget there's no factor 24 here, but 6.

Numbers:

  * $T = 2d 1h 8m = 72060s$
  * $M = 2.321\times10^{22}kg$
  * $r = 450 000m$

geostationnary orbit: 3 993.058 km


\newpage

## Distance between satellites in the relay

With :

  * $r$ = orbit of the satellites in meters
  * $n$ = number of satellites in the network

$$a = 180 - \frac{360}{n} $$
$$h = r \sin{\frac{a}{2}}$$
$$distance = 2 \sqrt{r^2 - h^2}$$

### Numerical application

Numbers:

  * $r = 3 993 058m$
  * $n = 3$

Distance between two satellites: 6 916.18 km

\newpage

## Required antennas

Maximum distance at which two vessels can connect:

  * $range$: range in meters
  * $v_1$: strength of vessel 1
  * $v_2$: strength of vessel 2

$$range = \sqrt{v_1 \times v_2}$$

With multiple antennas, the vessel strength can be obtained like that:

  * $v$: vessel strength
  * $P$: strongest antenna power
  * $t$: sum of all antennas powers

$$v = P \times \left(\frac{t}{P}\right)^{0.75}$$

### Numerical application

The distance between two satellites being 6 917 meters, we need at least that
strength. Since all satellites will be the same: 

  $$range = \sqrt{v \times v} = \sqrt{v^2} = v = strength$$

With two HG-5 relay antennas having a rating of 5M ($5\times10^6$m), we get this
range:

$$ 5\times10^6 \times \left(\frac{5\times10^6 * 2}{5\times10^6}\right)^{0.75} \approx 8408964$$

One antenna would have a range of 5 000km, two give us 8 408km, which is
enough.

\newpage

## Resonance orbit 

In order to inject the satellites evenly, we need a resonance orbit.
First we'll get the orbital period of such an orbit and later compute the
required apoapsis and periapsis.

### Period

  * $period$: orbital period of the circular orbit in seconds
  * $M$: mass of the planet in kg
  * $G$: gravitational constant, $6.67430 \times 10^{-11}$
  * $r$: radius of the planet in meters
  * $h$: desired injection orbit in meters
  * $n$: number of satellites

$$ period = 2 \pi \sqrt{\frac{(r+h)^3}{GM}}$$

The resonance orbit period is then:

$$ resonance = period + period \times \left(\frac{1}{n}\right)$$


### Apo and periapsis

The elliptical orbit is a bit eccentric, we need to get the semi-major axis:

  * $apoapsis$: apoapsis of the resonance orbit in meters
  * $sma$: semi-major axis in meters
  * $G$: gravitational constant, $6.67430 \times 10^{-11}$
  * $M$: mass of the planet in kg
  * $r$: radius of the planet in meters
  * $T$: orbital period of the resonance orbit in seconds
  * $h$: desired injection orbit in meters

$$sma = \sqrt[3]{\frac{GM \times T^2}{4 \pi^2}}$$

This equation will give us a _dive orbit_, meaning the injection orbit is the
periapsis:

$$ apoapsis = sma + (sma - h - 2r) $$


\newpage

### Numerical application

Numbers:

  * $M = 2.321\times10^{22}kg$
  * $r = 450 000m$
  * $h = 3 993 058m$

\begin{center}
Synchronous orbital period = 47 278 seconds (2d 1h 8m 0s)\\
Resonance orbital period = 63 037 seconds (2d 5h 30m 38s)
\end{center}

\begin{center}
periapsis = 3 993.058 km\\
apoapsis = 5 871.704 km
\end{center}

