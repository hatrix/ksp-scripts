from math import radians, sin, sqrt


def get_distance_satellites(radius_orbit, nb_satellites):
    r = radius_orbit
    
    alpha = (180 - 360 / nb_satellites) / 2
    h = r * sin(radians(alpha))
    distance = 2 * sqrt(r**2 - h**2)

    return distance


if __name__ == "__main__":
    # Synchronous orbit of Rhode
    r = 3993.058  # km
    distance = get_distance_satellites(r, 3)

    print(f'{round(distance, 2)} km')
