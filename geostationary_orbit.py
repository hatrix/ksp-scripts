from math import pi

def get_geostationary_altitude(mass_planet, rotation_period, radius_planet):
    G = 6.67384e-11

    r_3 = ((rotation_period**2) * (G) * (mass_planet)) / (4 * (pi**2))
    r = r_3 ** (1/3)
    
    h = r - radius_planet

    return h

def get_secs(string):
    d, h, m = [int(x) for x in string.split()]
    seconds = d * 6 * 60 * 60 + h * 60 * 60 + m * 60
    return seconds
    
if __name__ == "__main__":
    m_planet = float(input("Mass of the planet in kg: "))
    #rotation_period = get_secs(input("Rotation period of the planet in d h m s: "))
    rotation_period = float(input("Rotation period of the planet in seconds: "))
    radius = float(input("Radius of the planet in km: ")) * 1000

    orbit = get_geostationary_altitude(m_planet, rotation_period, radius)
    orbit_str = '{:,}'.format(orbit).replace(',', ' ')
    print(f"Geostationary orbit: {orbit_str} m")
